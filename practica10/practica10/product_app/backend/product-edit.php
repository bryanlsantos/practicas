<?php
    use api\Actualizar\Actualizar as Actualizar;
    require_once __DIR__.'/start.php';

    $productos = new Actualizar('marketzone');
    $productos->edit( json_decode( json_encode($_POST) ) );
    echo $productos->getResponse();
?>