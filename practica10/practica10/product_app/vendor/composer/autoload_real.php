<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInita8b1afe6a0117fa7df640fd6ce9d69ba
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(array('ComposerAutoloaderInita8b1afe6a0117fa7df640fd6ce9d69ba', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInita8b1afe6a0117fa7df640fd6ce9d69ba', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        \Composer\Autoload\ComposerStaticInita8b1afe6a0117fa7df640fd6ce9d69ba::getInitializer($loader)();

        $loader->register(true);

        return $loader;
    }
}
