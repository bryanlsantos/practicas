<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<?php
$variable1=" PHP 5";
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es“>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
echo "<title>Una pagina llena de scripts de la practica de PHP</title>";
?>
</head>

<body>



<?php
echo "<h1>1.</h1> <h3>Determina cuál de las siguientes variables son válidas y explica por qué:</h3>";
$_myvar = " _myvar Es valida porque empieza con un '_' 
y todos sus caracteres con validos";
$_7var = " _7var Es valida porque empieza con un '_' 
y todos sus caracteres con validos";
$myvar = "myvar Si es valida porque si tiene '$' al inicio
y todos sus caracteres con validos";
$var7  = "var7 Si es valida porque empieza con v al inicio
y todos sus caracteres con validos";
$_element1 = "_element1 Si es valida porque empieza con un '_'
y todos sus caracteres con validos";

echo "<p> $_myvar </p>";
echo "<p> $_7var </p>";
echo "<p> myvar No es Valido porque no tiene '$' </p>";
echo "<p> $var7 </p>";
echo "<p> $_element1 </p>";
echo "<p> house*5 No es valido porque contiene un '*' </p>";

echo "<h1>2. </h1><h3>Proporcionar los valores de a, b, c como sigue:</h3>";
$a = "ManejadorSQL";
$b = 'MySQL';
$c = &$a;

echo "<h1>BLOQUE 1</h1>";
echo "<p> a = $a </p>";
echo "<p> b = $b </p>";
echo "<p> c = $c </p>";

$a = "PHP server";
$b = &$a;
echo "<h1>BLOQUE 2</h1>";
echo "<p> a = $a </p>";
echo "<p> b = $b </p>";
echo "<p> c = $c </p>";

echo "<p>Lo que paso en el segundo bloque fué de que al asignar b = a
hace que 'b' 'tenga' el valor de 'a' en otras palabras, se puede decir que 'b' apunta a 'a'. 
Haciendo un resultado donde 'a' sea PHP server y que 'c' y 'b' apuntando a 
'a' hace que las tres variables sean PHP server.</p>";

unset($a);
unset($b);
unset($c);

echo "<h1>3. </h1><h3>Muestra el contenido de cada variable inmediatamente después de cada asignación,
verificar la evolución del tipo de estas variables (imprime todos los componentes de los arreglo):</h3>";
$a = "PHP5";
echo "<p>a = $a</p>";
$z[] = &$a;
echo "z = ";
print_r($z);
var_dump($z);
$b = "5a version de PHP";
echo "<p>b = $b</p>";
$c = ($b*10);
echo "<p>c = $c</p>";
$a .= $b;
echo "<p>a = $a</p>";
$b *= $c;
echo "<p>b = $b</p>";
$z[0] = "MySQL";
echo "z = ";
print_r($z);
var_dump($z);
echo "<p>a = $a</p>";
echo "<p>b = $b</p>";
echo "<p>c = $c</p>";


echo "<h1>4. </h1><h3>Lee y muestra los valores de las variables del ejercicio anterior, pero ahora con la ayuda de
la matriz GLOBALS o del modificador global de PHP.</h3>";
function Mostrar()
{
	echo "<h1>Entrando a funcion Mostrar()</h1>";
	global $a, $b, $c, $z;
	echo "<p>a = $a</p>";
	echo "<p>b = $b</p>";
	echo "<p>c = $c</p>";
	echo "z = ";
	print_r($z);
}
Mostrar();


unset($a);
unset($b);
unset($c);
unset($z);

echo "<h1>5. </h1><h3>Dar el valor de las variables a, b, c al final del siguiente script:</h3>";
$a = "7 personas";
echo "<p>a = $a</p>";
$b = (integer) $a;
echo "<p>b = $b</p>";
$a = "9E3";
echo "<p>a = $a</p>";
$c = (double) $a;
echo "<p>a = $a</p>";
echo "<p>b = $b</p>";
echo "<p>c = $c</p>";


unset($a);
unset($b);
unset($c);

echo "<h1>6. </h1><h3>Dar y comprobar el valor booleano de las variables a, b, c, d, e y f y muéstralas usando la función var_dump(<datos>). Después investiga una función de PHP que permita transformar el valor booleano de c y e en uno que se pueda mostrar con un echo: </h3>";
$a = "0";
$b = "TRUE";
$c = FALSE;
$d = ($a OR $b);
$e = ($a AND $c);
$f = ($a XOR $b);

echo "a = ";
var_dump($a);
echo "<p>\n</p>";

echo "b = ";
var_dump($b);
echo "<p>\n</p>";

echo "c = ";
var_dump($c);
echo "<p>\n</p>";

echo "d = ";
var_dump($d);
echo "<p>\n</p>";

echo "e = ";
var_dump($e);
echo "<p>\n</p>";

echo "f = ";
var_dump($f);
echo "<p>\n</p>";

echo "c= ";
echo json_encode($c);
echo "<br>";
echo "<br>";

echo "e= ";
echo json_encode($e);
echo "<br>";

//settype($c, "string");

unset($a);
unset($b);
unset($c);
unset($d);
unset($e);
unset($f);

echo "<h1>7.</h1><h3>Usando la variable predefinida _SERVER, determina lo siguiente:</h3>";
$informacionapa =  $_SERVER['SERVER_SOFTWARE'];
echo "\n\n\nLa version de apache y php es  $informacionapa";
echo "<br><br>";

$informacionso = $_SERVER['HTTP_USER_AGENT'];
echo "La informacion del sistema operativo es  $informacionso";
echo "<br><br>";

$idioma = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
echo "El idioma en el que tienes tu navegador es: $idioma ";
echo "<br><br>";

?>


</body>