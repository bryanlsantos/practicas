<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es“>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
echo "<title>Una pagina con las respuestas de la practica 4 de PHP</title>";
?>
</head>

<body>
<?php
//----------------------------- INCISO 1-------------------------------------
function inciso1(){
    echo "<h1>Inciso 1</h1><br>";
    $numero = $_GET["numero"];
    if ($numero%5==0 && $numero%7==0)
    {
        echo "El numero es multiplo de 5 y 7";
    }
    else
    {
        echo "El numero no es multiplo de 5 y 7";
    }
}
inciso1();
//----------------------------- INCISO 2-------------------------------------
function inciso2(){
    echo "<br><br><h1>Inciso 2</h1>";
    $lista = [[],[],[]];
    //$aux1 = 0;
    //$aux2 = 0;
    //$aux3 = 0;
    $condicion = false;
    //$arreglo = [];
    $contador = 0;
    do{
        $contador = $contador + 1;
        //global $condicion, $lista;
        $aux1 = rand(1, 20);
        //$arreglo[] = $aux1;
        $aux2 = rand(1, 20);
        //$arreglo[] = $aux2;
        $aux3 = rand(1, 20);
        //$arreglo[] = $aux3;
        $lista[0][] = $aux1;
        $lista[1][] = $aux2;
        $lista[2][] = $aux3;
        if($aux1%2!=0 && $aux2%2==0 && $aux3%2!=0) 
        {
            $condicion = true;
        }
    }while($condicion==false);
    /*	
    var_dump($lista[0]);
    echo "<br>";
    var_dump($lista[1]);
    echo "<br>";
    var_dump($lista[2]);
    */

    echo "<br>";

    for($k=0;$k<$contador;$k++){
        foreach($lista as $clave => $num) {
                echo $num[$k]."\n";
        } 
        echo "<br>";
    }
    /*   2da opcion
    $determinante = 1;
    for($i=0; $i<=count($arreglo)-1; $i++)
    {
        if($determinante%3==0) {
            echo $arreglo[$i]."\n";
            echo "<br>";
        }
        else {
            echo $arreglo[$i]."\n";
        }
        $determinante = $determinante + 1;
    } */
    $numeros = $contador*3; 
    echo $numeros." numeros obtenidos de ".$contador." iteraciones";
}
inciso2();
//----------------------------- INCISO 3-------------------------------------
    function inciso3(){
    echo "<br><br><h1>Inciso 3</h1>";
    $numero2 = $_GET["numero2"];
    echo "Echo con while: <br>";
    $cond = false;
    while($cond==false){
        $aleatorio = rand();
        if($aleatorio%$numero2==0){
            echo "El numero ".$aleatorio." es un numero multiplo de " .$numero2;
            $cond=true;
        }
        else{}
    }
    echo "<br><br><br> Variando con do while: <br>";
    $cond2 = false;
    do {
        $aleatorio2 = rand();
        if($aleatorio2%$numero2==0){
            echo "El numero ".$aleatorio2." es un numero multiplo de " .$numero2;
            $cond2=true;
        }
        else{}
    }while($cond2==false);

    // $listab = [[],[],[]];
    // $condicionb = false;
    // $contadorb = 0;
    // while($condicionb==false){
    //     $contadorb++;
    // 	$aux1b = rand(1, 20);
    // 	$aux2b = rand(1, 20);
    // 	$aux3b = rand(1, 20);
    //     $listab[0][] = $aux1b;
    // 	$listab[1][] = $aux2b;
    // 	$listab[2][] = $aux3b;
    // 	if(($aux1b%2!=0 && $aux2b%2==0 && $aux3b%2!=0) || $listab[0][0]%$numero2==0) // AGREGAMOS LA CONDICION DEL NUMERO SEA MULTIPLO
    // 	{
    // 		$condicionb = true;
    //         echo "Listo!!!";
    // 	}
    // }
    // echo "<br>";
    // echo "<br>";

    // for($k=0;$k<$contadorb;$k++){
    //     foreach($listab as $clave => $num) {
    //             echo $num[$k]."\n";
    //     } 
    //     echo "<br>";
    // }
}
inciso3();
//----------------------------- INCISO 4-------------------------------------
    function inciso4(){
    echo "<br><br><h1>Inciso 4</h1>";
    //$indices[] = [];
    for($i = 97; $i<123; $i++) 
    {
        $indices[$i] = chr($i);

    }

    echo '<br> <div> <table border="2" >';
    echo "<tr><th>Numero</th><th>Letra</th> </tr>";
    foreach ($indices as $letras => $letra) {
        
        echo "<tr>";
        echo "<th>".$letras."</th>";
        echo"<th>".$letra."</th>"; 
        echo "</tr>";

    }
    echo "</table> </div>";
    }
inciso4();
?>

</body>