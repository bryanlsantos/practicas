<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es“>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
echo "<title>Una pagina con las respuestas de la practica 4 de PHP</title>";
?>
</head>

<body>
<?php
//----------------------------- INCISO 6-------------------------------------
echo "<h1>Inciso 6</h1>";
$autos["ABC1090"] = array("Auto"=> array("marca"=> "HONDA", "modelo" => "2010", "tipo"=> "camioneta" ), "Propietario" => array("nombre" => "Lilia Esparza", "ciudad"=>"Puebla, Pue", "direccion"=>"C.U., Jardines de San Manuel") );
$autos["ABC2090"] = array("Auto"=> array("marca"=> "NISSAN", "modelo" => "2011", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Federico Guevara", "ciudad"=>"Apizaco, Tlax", "direccion"=>"Centro, Francisco Sarabia") );
$autos["ABC3090"] = array("Auto"=> array("marca"=> "MITSUBISHI", "modelo" => "2016", "tipo"=> "camioneta" ), "Propietario" => array("nombre" => "Beto Sierra", "ciudad"=>"Puebla, Pue", "direccion"=>"Centro, 31 Sur") );
$autos["GHK5708"] = array("Auto"=> array("marca"=> "CHEVROLET", "modelo" => "2020", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Marco Guzman", "ciudad"=>"Apizaco, Tlax", "direccion"=>"San Martin, Avenida Moctezuma") );
$autos["JHK5708"] = array("Auto"=> array("marca"=> "VOLKSWAGEN", "modelo" => "2022", "tipo"=> "hachback" ), "Propietario" => array("nombre" => "Sofia Lara", "ciudad"=>"Apan, Hgo", "direccion"=>"El Carmen, Jesus Carranza") );
$autos["KKK1505"] = array("Auto"=> array("marca"=> "BMW", "modelo" => "2019", "tipo"=> "camioneta" ), "Propietario" => array("nombre" => "Jonatan Perez", "ciudad"=>"Pachuca, Hgo", "direccion"=>"Santa Anna, Francisco Imadero") );
$autos["WWW1940"] = array("Auto"=> array("marca"=> "AUDI", "modelo" => "2015", "tipo"=> "camioneta" ), "Propietario" => array("nombre" => "Fernanda Flores", "ciudad"=>"San Martin, Pue", "direccion"=>"San Pedro, Aquiles Serdan") );
$autos["DEU9909"] = array("Auto"=> array("marca"=> "SUSUKI", "modelo" => "2009", "tipo"=> "hachback" ), "Propietario" => array("nombre" => "Peter Parker", "ciudad"=>"Pachuca, Hgo", "direccion"=>"Xicontecatl, Jesus Carranza") );
$autos["MHK5708"] = array("Auto"=> array("marca"=> "TOYOTA", "modelo" => "2001", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Bruno Diaz", "ciudad"=>"Tulancingo, Hgo", "direccion"=>"Loma Verde, 22 de Noviembre") );
$autos["IQA7609"] = array("Auto"=> array("marca"=> "HONDA", "modelo" => "1999", "tipo"=> "camioneta" ), "Propietario" => array("nombre" => "Barry Allen", "ciudad"=>"Tecla, Tlaxcala", "direccion"=>"Emilio Zanchez, 9 de Agosto") );
$autos["EHA5009"] = array("Auto"=> array("marca"=> "NISSAN", "modelo" => "2007", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Jhony Bravo", "ciudad"=>"Mazatlan, Sinaloa", "direccion"=>"Enrique de Juarez, 9 de Abril") );
$autos["KBA3809"] = array("Auto"=> array("marca"=> "CHEVROLET", "modelo" => "2009", "tipo"=> "camioneta" ), "Propietario" => array("nombre" => "Chuck Norris", "ciudad"=>"Tequila, Jalisco", "direccion"=>"Revolucion, Josefa Ortiz") );
$autos["GGA3090"] = array("Auto"=> array("marca"=> "VOLKSWAGEN", "modelo" => "2010", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Sasha Gris", "ciudad"=>"Guadalajara, Jalisco", "direccion"=>"Centro, 22 de Septiembre") );
$autos["MBA2999"] = array("Auto"=> array("marca"=> "AUDI", "modelo" => "2010", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Pedro Guzman", "ciudad"=>"Coyoacan, CDMX", "direccion"=>"Madero, Turquesa") );
$autos["GYA3809"] = array("Auto"=> array("marca"=> "BMW", "modelo" => "2001", "tipo"=> "sedan" ), "Propietario" => array("nombre" => "Eren Jeager", "ciudad"=>"Tangamangapio, Michoacan", "direccion"=>"Centro, 11 de Noviembre") );
?>

<pre>
<?php print_r($autos); ?>
</pre>
</body>
<form action="" method="post">
Que matricula deseas consultar: <input type="text" name="matricula"><br>
<input type="submit">
</form>
<br>
<?php
    if(empty($_POST["matricula"])==true){}
    else{
        $auxiliarmatricula = $_POST["matricula"];
        if($_POST["matricula"] == "todos")
        { 
            echo "Bienvenido, usted buscó todas las matriculas <br>";
            echo "Estos son sus datos";
            echo '<pre>';
            print_r($autos);
            echo '</pre>';
        }
        else {
            echo "Bienvenido, usted buscó la matricula: ".$_POST["matricula"]."<br>";
            echo "Estos son sus datos:<br>";
            print_r($autos[$auxiliarmatricula]);
        }
    }

?>
</html>
