 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 20px; 
			background-color: #C4DF9B;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: #005825;
			border-bottom: 1px solid #005825;}
			h2 {font-size: 1.2em;
			color: #4A0048;}
		</style>
	</head>
	<body>
<?php 
//$nombre = ;
//$marca  = 'marca_producto';
//$modelo = 'modelo_producto';
//$precio = 1.0;
//$detalles = 'detalles_producto';
//$unidades = 1;
//$imagen   = 'img/imagen.png';

/** SE CREA EL OBJETO DE CONEXION */
@$link = new mysqli('localhost', 'root', 'Elkomander', 'marketzone');	

/** comprobar la conexión */
if ($link->connect_errno) 
{
    die('Falló la conexión: '.$link->connect_error.'<br/>');
    /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
}

$nombre = $_POST['name_product'];
$marca = $_POST['brand_product'];
$modelo = $_POST['model_product'];
$precio = floatval($_POST['price_product']);
$detalles = $_POST['details_product'];
$unidades = intval($_POST['quantity_product']);
$imagen = $_POST['image_product'];
$errorestipos = '';
$errorcondicion = false;
$condicionvacios = false;
if(empty($_POST['name_product'])) { 
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con el producto, asegurate de escribir el nombre del producto <br>';
}
if(empty($_POST['brand_product'])) {
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con la marca del producto, asegurate de escribir la marca del producto <br>';
}
if(empty($_POST['model_product'])) {
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con el modelo del producto, asegurate de escribir el modelo del producto <br>';
}
if(empty($_POST['price_product'])) {
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con el precio del producto, asegurate de escribir el precio del producto <br>';
}
if(empty($_POST['details_product'])) {
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con los detalles del producto, asegurate de escribir los detalles del producto <br>';
}
if(empty($_POST['quantity_product'])) {
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con la cantidad del producto, asegurate de escribir la cantidad del producto <br>';
}
if(empty($_POST['image_product'])) { 
    $condicionvacios = true;
    $errorestipos .= 'Error =(, Hubo un error con la imagen del producto, asegurate de escribir el nombre de la imagen del producto <br>';
}
if(!is_string( $nombre) ) {
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con el nombre del producto, asegurate de escribir bien el nombre del producto <br>';
}
if(!is_string( $marca) )
{
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con la marca del producto, asegurate de escribir bien la marca del producto <br>';
}
if(!is_string( $modelo) ){
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con el modelo del producto, asegurate de escribir bien el modelo <br>';
}
if(!is_float( $precio)){
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con el precio del producto, asegurate de escribir bien el numero de precio del producto <br>'; 
}
if(!is_string( $detalles)){
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con los detalles, asegurate de escribir bien los detalles del producto <br>';
}
if(!is_int( $unidades)) {
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con las unidades, asegurate de escribir bien el numero de las unidades <br>';
}
if(!is_string( $imagen)){ 
    $errorcondicion = true; 
    $errorestipos .= 'Error =(, Hubo un error con la imagen, asegurate de escribir bien el nombre de la imagen <br>';
}
    if($errorcondicion == false and $condicionvacios == false) {
        $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', '0')";
        if ( $link->query($sql) ) 
        {
            echo '<h1>Informacion del producto:</h1><br>';
            echo 'Producto con ID: '.$link->insert_id;
            echo '<br>Nombre del producto: ' . $nombre . '<br>';
            echo 'Marca del producto: ' . $marca . '<br>';
            echo 'Modelo del producto: ' . $nombre . '<br>';
            echo 'Precio del producto: ' . $precio . '<br>'; 
            echo 'Detalles del producto: ' . $detalles . '<br>';
            echo 'Unidades del producto: ' . $unidades . '<br>';
            echo 'Imagen del producto: ' .$imagen . '<br>';
        }
        else
        {
            echo 'El Producto no pudo ser insertado =(';
        }
    }                  
    else{
        echo ($errorestipos);
    }

$link->close();
?>
</body>
</html>