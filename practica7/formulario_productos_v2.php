<!DOCTYPE html >
<html>

  <head>
    <meta charset="utf-8" >
    <title>Registro de productos</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>
  </head>

  <body>
    <h1>Registro de Productos</h1>

    <p>Ingresa los datos del producto a registrar</p>

    <form id="formularioProducto" action="http://localhost:8080/tecnologiasweb/practicas/p06/set_producto_v2.php" method="post">


      <fieldset>
        <legend>Información del Producto</legend>

        <ul>
          <li><label for="form-name_product">Nombre del producto: </label> <input type="text" name="name_product" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>" id="form-name_product"></li>
          <li><label for="form-brand_product">Marca: </label> <input type="text" name="brand_product" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>" id="form-brand_product"></li>
          <li><label for="form-model_product">Modelo: </label> <input type="text" name="model_product" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>" id="form-model_product"></li>
          <li><label for="form-price_product">Precio: </label><input type="number" min="99.99" name="price_product" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>" id="form-price_product">
          <li><label for="form-details_product">Detalles: </label><br><textarea name="details_product" rows="4" cols="60" id="form-details_product" placeholder="No más de 200 caracteres de longitud" ><?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?></textarea></li>
          <li><label for="form-quantity_product">Unidades: (Numero) </label><input type="number" min="0" name="quantity_product" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>" id="form-quantity_product">
          <li><label for="form-image_product">Imagen: </label><input type="text" name="image_product" value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>" id="form-image_product">
        </ul>
      </fieldset>

      <p>
        <input type="submit" value="Agregar Producto">
        <input type="reset">
      </p>

    </form>
   
  </body>
</html>