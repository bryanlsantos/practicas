<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<?php
    $data = array();

	if(isset($_GET['eliminado']))
    {
		$eliminado = $_GET['eliminado'];
    }
    else
    {
        die('Parámetro "eliminado" no detectado...');
    }

	if (!empty($eliminado))
	{
		/** SE CREA EL OBJETO DE CONEXION */
		@$link = new mysqli('localhost', 'root', 'Elkomander', 'marketzone');
        /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

		/** comprobar la conexión */
		if ($link->connect_errno) 
		{
			die('Falló la conexión: '.$link->connect_error.'<br/>');
			//exit();
		}

		/** Crear una tabla que no devuelve un conjunto de resultados */
		if ( $result = $link->query("SELECT * FROM productos WHERE eliminado = 0") ) 
		{
            $fildatos = "SELECT * FROM productos WHERE eliminado = 0";
            /** Se extraen las tuplas obtenidas de la consulta */
			$row = $result->fetch_array(MYSQLI_ASSOC);
            $consulta = mysqli_query($link,$fildatos);

            // /** Se crea un arreglo con la estructura deseada */
            // foreach($row as $num => $registro) {            // Se recorren tuplas
            //     foreach($registro as $key => $value) {      // Se recorren campos
            //         $data[$num][$key] = utf8_encode($value);
            //     }
            // }

			/** útil para liberar memoria asociada a un resultado con demasiada información */
		
		}

		$link->close();

	}
	?>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet"
              href= "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
              integrity= "sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
              crossorigin="anonymous" />
	</head>
	<script>
		function show() {
            // se obtiene el id de la fila donde está el botón presinado
            var rowId = event.target.parentNode.parentNode.id;
            // se obtienen los datos de la fila en forma de arreglo
            var data = document.getElementById(rowId).querySelectorAll(".row-data");
            /**
            querySelectorAll() devuelve una lista de elementos (NodeList) que 
            coinciden con el grupo de selectores CSS indicados.
            (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)
            En este caso se obtienen todos los datos de la fila con el id encontrado
            y que pertenecen a la clase "row-data".
            */
			var id = data[0].innerHTML;
            var nombre = data[1].innerHTML;
			var marca = data[2].innerHTML;
			var modelo = data[3].innerHTML;
			var precio = data[4].innerHTML;
			var detalles = data[5].innerHTML;
			var unidades = data[6].innerHTML;
            var imagensrc = data[7].firstChild.getAttribute('src');
            var imagen = imagensrc.substring(4);
            alert("Nombre: " + nombre + "\nMarca: " + marca + "\nModelo: " + modelo + "\nPrecio: " + precio + "\nDetalles: "  + detalles + "\nUnidades: " + unidades + "\nImagen: " + imagen);
            send2form(id, nombre, marca, modelo, precio, detalles, unidades, imagen);
		}

		function send2form(id, nombre, marca, modelo, precio, detalles, unidades, imagen) {    
                var urlForm = "formulario_productos_v3.php";
				var propId = "id="+id;
                var propNombre = "nombre="+nombre;
                var propMarca = "marca="+marca;
				var propModelo = "modelo="+modelo;
				var propPrecio = "precio="+precio;
				var propDetalles = "detalles="+detalles;
				var propUnidades = "unidades="+unidades;
				var propImagen = "imagen="+imagen;
				window.open(urlForm+"?"+propId+"&"+propNombre+"&"+propMarca+"&"+propModelo+"&"+propPrecio+"&"+propDetalles+"&"+propUnidades+"&"+propImagen);
            }	
	</script>

    <body>

		<h3>PRODUCTO</h3>

		<br/>
		
		<?php if( isset($row) ) : ?>

			<table class="table">
			<tbody>
				<thead class="thead-dark">
						<tr>
						<th scope="col">#</th>
						<th scope="col">Nombre</th>
						<th scope="col">Marca</th>
						<th scope="col">Modelo</th>
						<th scope="col">Precio</th>
						<th scope="col">Detalles</th>
						<th scope="col">Unidades</th>
						<th scope="col">Imagen</th>
						<th scope="col">Modificar</th>
						</tr>
				</thead>
				
                    <?php 
                    foreach($consulta as $coldatos) { ?>
					<tr id="<?=$coldatos['id']?>" > 
						<td class="row-data"><?=$coldatos['id']?></td>
						<td class="row-data"><?= $coldatos['nombre'] ?></td>
						<td class="row-data"><?= $coldatos['marca'] ?></td>
						<td class="row-data"><?= $coldatos['modelo'] ?></td>
						<td class="row-data"><?= $coldatos['precio'] ?></td>
						<td class="row-data"><?= utf8_encode($coldatos['detalles']) ?></td>
						<td class="row-data"><?= $coldatos['unidades'] ?></td>
						<td class="row-data"><img width="200px" src=img/<?= $coldatos['imagen'] ?> ></td>
						<td ><br><br><input type="button" onclick="show()" value="Modificar"/></td>
					</tr>
                <?php } ?>
			</tbody>
			</table>
        <?php endif; ?>
	</body>	 
</html>



