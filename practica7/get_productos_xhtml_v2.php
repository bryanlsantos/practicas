<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<?php
    $data = array();

	if(isset($_GET['tope']))
    {
		$tope = $_GET['tope'];
    }
    else
    {
        die('Parámetro "tope" no detectado...');
    }

	if (!empty($tope))
	{
		/** SE CREA EL OBJETO DE CONEXION */
		@$link = new mysqli('localhost', 'root', 'Elkomander', 'marketzone');
        /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

		/** comprobar la conexión */
		if ($link->connect_errno) 
		{
			die('Falló la conexión: '.$link->connect_error.'<br/>');
			//exit();
		}

		/** Crear una tabla que no devuelve un conjunto de resultados */
		if ( $result = $link->query("SELECT * FROM productos WHERE unidades <= $tope") ) 
		{
            $fildatos = "SELECT * FROM productos WHERE unidades <= $tope";
            /** Se extraen las tuplas obtenidas de la consulta */
			$row = $result->fetch_array(MYSQLI_ASSOC);
            $consulta = mysqli_query($link,$fildatos);

            // /** Se crea un arreglo con la estructura deseada */
            // foreach($row as $num => $registro) {            // Se recorren tuplas
            //     foreach($registro as $key => $value) {      // Se recorren campos
            //         $data[$num][$key] = utf8_encode($value);
            //     }
            // }

			/** útil para liberar memoria asociada a un resultado con demasiada información */
		
		}

		$link->close();

	}
	?>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<script>
		 function show() {
                // se obtiene el id de la fila donde está el botón presinado
                var rowId = event.target.parentNode.parentNode.id;

                // se obtienen los datos de la fila en forma de arreglo
                var data = document.getElementById(rowId).querySelectorAll(".row-data");
                /**
                querySelectorAll() devuelve una lista de elementos (NodeList) que 
                coinciden con el grupo de selectores CSS indicados.
                (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

                En este caso se obtienen todos los datos de la fila con el id encontrado
                y que pertenecen a la clase "row-data".
                */

                var nombre = data[0].innerHTML;
                var marca = data[1].innerHTML;
				var modelo = data[2].innerHTML;
				var precio = data[3].innerHTML;
				var detalles = data[4].innerHTML;
				var unidades = data[5].innerHTML;
                var imagensrc = data[6].firstChild.getAttribute('src');
                var imagen = imagensrc.substring(4);

				// var imagenpre = data[6].innerHTML;
				// var lugar =  imagenpre.search(".png");
				// var imagen = imagenpre.slice(0, lugar+4);


                alert("Nombre: " + nombre + "\nMarca: " + marca + "\nModelo: " 
				+ modelo + "\nPrecio: " + precio + "\nUnidades: " + unidades + 
				"\nDetalles: " + detalles + "\nImagen: " + imagen );

                send2form(nombre, marca, modelo, precio, detalles, unidades, imagen);
            }

            function send2form(nombre, marca, modelo, precio, detalles, unidades, imagen) {
                var form = document.createElement("form");

                var nombreIn = document.createElement("input");
                nombreIn.type = 'text';
                nombreIn.name = 'nombre';
                nombreIn.value = nombre;
                form.appendChild(nombreIn);

                var marcaIn = document.createElement("input");
                marcaIn.type = 'text';
                marcaIn.name = 'marca';
                marcaIn.value = marca;
                form.appendChild(marcaIn);

				var modeloIn = document.createElement("input");
                modeloIn.type = 'text';
                modeloIn.name = 'modelo';
                modeloIn.value = modelo;
                form.appendChild(modeloIn);

				var precioIn = document.createElement("input");
                precioIn.type = 'number';
                precioIn.name = 'precio';
                precioIn.value = precio;
                form.appendChild(precioIn);

				var detallesIn = document.createElement("input");
                detallesIn.type = 'text';
                detallesIn.name = 'detalles';
                detallesIn.value = detalles;
                form.appendChild(detallesIn);

				var unidadesIn = document.createElement("input");
                unidadesIn.type = 'number';
                unidadesIn.name = 'unidades';
                unidadesIn.value = unidades;
                form.appendChild(unidadesIn);
			
				var imagenIn = document.createElement("input");
                imagenIn.type = 'text';
                imagenIn.name = 'imagen';
                imagenIn.value = imagen;
                form.appendChild(imagenIn);

				console.log(form);

                form.method = 'POST';
                form.action = 'formulario_productos_v2.php';  

                document.body.appendChild(form);
                form.submit();
            }
    </script>

    <body>

		<h3>PRODUCTO</h3>

		<br/>
		
		<?php if( isset($row) ) : ?>

			<table class="table">
			<tbody>
				<thead class="thead-dark">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Detalles</th>
					<th scope="col">Unidades</th>
					<th scope="col">Imagen</th>
					<th scope="col">Modificar</th>
					</tr>
				</thead>
				
                    <?php 
                    foreach($consulta as $coldatos) { ?>
					<tr id="<?=$coldatos['id']?>" > 
						<th scope="row"><?= $coldatos['id'] ?></th>
						<td class="row-data"><?= $coldatos['nombre'] ?></td>
						<td class="row-data"><?= $coldatos['marca'] ?></td>
						<td class="row-data"><?= $coldatos['modelo'] ?></td>
						<td class="row-data"><?= $coldatos['precio'] ?></td>
						<td class="row-data"><?= utf8_encode($coldatos['detalles']) ?></td>
						<td class="row-data"><?= $coldatos['unidades'] ?></td>
						<td class="row-data"><img width="200px" src=img/<?= $coldatos['imagen'] ?> ></td>
						<td ><br><br><input type="button" onclick="show()" value="Modificar"/></td>
					</tr>
                    <?php } ?>
				</tbody>
			</table>
        <?php endif; ?>
    </body>
</html>



