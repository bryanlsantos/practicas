// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "imagen-default.png"
  };

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarID(e) {
    /**
     * Revisar la siguiente información para entender porqué usar event.preventDefault();
     * http://qbit.com.mx/blog/2013/01/07/la-diferencia-entre-return-false-preventdefault-y-stoppropagation-en-jquery/#:~:text=PreventDefault()%20se%20utiliza%20para,escuche%20a%20trav%C3%A9s%20del%20DOM
     * https://www.geeksforgeeks.org/when-to-use-preventdefault-vs-return-false-in-javascript/
     */
    e.preventDefault();

    // SE OBTIENE EL ID A BUSCAR
    var id = document.getElementById('search').value;

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n'+client.responseText);
            
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText);    // similar a eval('('+client.responseText+')');
            let template = '';

            for(var i = 0; i<productos.length; i++){
                // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                let descripcion = '';
                descripcion += '<li>precio: '+productos[i].precio+'</li>';
                descripcion += '<li>unidades: '+productos[i].unidades+'</li>';
                descripcion += '<li>modelo: '+productos[i].modelo+'</li>';
                descripcion += '<li>marca: '+productos[i].marca+'</li>';
                descripcion += '<li>detalles: '+productos[i].detalles+'</li>';
                    
                // SE CREA UNA PLANTILLA PARA CREAR LA(S) FILA(S) A INSERTAR EN EL DOCUMENTO HTML
                template += `
                    <tr>
                        <td>${productos[i].id}</td>
                        <td>${productos[i].nombre}</td>
                        <td><ul>${descripcion}</ul></td>
                    </tr>
                `;
            }
                // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                document.getElementById("productos").innerHTML = template;
        }
    };
    client.send("id="+id);
}

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();

    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;
    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);
    
    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;
    var error = false;
    var mensajeerror = "";
    if(finalJSON["nombre"].length == 0 )
    {
        error = true;
        mensajeerror += "\nAgrega el nombre del producto";
    }
    if(finalJSON["nombre"].length > 100)
    {
        error = true;
        mensajeerror += "\nEscribe el nombre del producto con menos de 150 caracteres";
    }
    if(finalJSON["modelo"].length == 0)
    {
        error = true;
        mensajeerror += "\nAgrega el modelo del producto";
    }
    if(finalJSON["modelo"].length > 25)
    {
        error = true;
        mensajeerror += "\nEscribe el nombre del producto con menos de 26 caracteres";
    }
    if(finalJSON["marca"].length == 0)
    {
        error = true;
        mensajeerror += "\nAgrega la marca del producto";
    }
    if(finalJSON["modelo"].length > 25)
    {
        error = true;
        mensajeerror += "\nEscribe la marca del producto con menos de 26 caracteres";
    }
    if(finalJSON["precio"].length == 0)
    {
        error = true;
        mensajeerror += "\nAgrega el precio del producto";
    }
    if(finalJSON["precio"] < 99.99)
    {
        error = true;
        mensajeerror += "\nEscribe el precio del producto con mas de $99.99";
    }
    if(finalJSON["detalles"].length > 250)
    {
        error = true;
        mensajeerror += "\nEscribe los detalles del producto con 250 caracteres o menos";
    }
    if(finalJSON["unidades"] == "")
    {
        error = true;
        mensajeerror += "\nEscribe las unidades del producto con mas de 0";
    }
    if(finalJSON["unidades"] < 0)
    {
        error = true;
        mensajeerror += "\nEscribe las unidades del producto con mas de 0";
    }
   

    // SE OBTIENE EL STRING DEL JSON FINAL
    productoJsonString = JSON.stringify(finalJSON,null,2);

    if(finalJSON["imagen"] == "")
    {
        finalJSON.imagen.value = "imagen-default.png";
    }

    if(error == true) {
        alert(mensajeerror);
    }
    if(error == false) {
    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/create.php', true);
    client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log(client.responseText);
            console.log(String(client.responseText));
            window.alert(String(client.responseText));
        }
    };
    client.send(productoJsonString);
    
    }
    
}

// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try{
        objetoAjax = new XMLHttpRequest();
    }catch(err1){
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try{
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(err2){
            try{
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(err3){
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}