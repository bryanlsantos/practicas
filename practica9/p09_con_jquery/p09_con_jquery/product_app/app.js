 // JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;

   
}

$(document).ready(function(){
    let edit = false;
    console.log('JQuery is working');
    fetchProducts();
    $('#product-result').hide();

    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
            url: 'backend/product-search.php', 
            type: 'GET',
            data: { search },
            success: function(response) {
                let productos = JSON.parse(response);
                console.log(JSON.parse(response));
                let template = '';

                productos.forEach(producto => {
                    template += `<li>
                    ${producto.nombre}
                </li>`
                });

                $('#container').html(template);
                $('#product-result').show();
              
            }
        });
    }
    });


    $('#product-form').submit(function (e) {
        var bool=confirm("Seguro de agregar/modificar el dato el dato?");
        if(bool){
            
            let datos = JSON.parse($('#description').val());
            const postData = {
                nombre: $('#name').val(),
                precio: datos["precio"],
                unidades: datos["unidades"],
                modelo: datos["modelo"],
                marca: datos["marca"],
                detalles: datos["detalles"],
                imagen: datos["imagen"],
                id: $('#productId').val()
        };
            const url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
            console.log(postData, url);
            productoJsonString = JSON.stringify(postData,null,2);
            if(url == 'backend/product-add.php'){
                $.post(url, productoJsonString, function(response) {
                    alert(response);
                    console.log(response);
                    fetchProducts();
                    $('#product-form').trigger('reset');
                    });
            }
            else{
                $.post(url, postData, function(response) {
                    alert(response);
                    console.log(response);
                    fetchProducts();
                    $('#product-form').trigger('reset');
                    });
            }
        }else{
            alert("Cancelo la solicitud");
        }
        
        e.preventDefault;
    });

    function fetchProducts() {
        $.ajax({
            url: 'backend/product-list.php',
            type: 'POST',
            success: function (response) {
                let products = JSON.parse(response);
                let template = '';
                console.log(response);
                products.forEach(product => {
                    template += `
                    <tr productId="${product.id}">
                        <td>${product.id}</td>
                        <td>
                            <a href='#' class="product-item">${product.nombre}</a>
                        </td>
                        <td>
                        Precio: ${product.precio} 
                        Unidades: ${product.unidades}
                        Modelo: ${product.modelo}
                        Marca: ${product.marca}
                        Detalles: ${product.detalles}
                        </td>
                        <td>
                            <button class="product-delete btn btn-danger"> 
                            Eliminar
                            </button>
                        </td>
                    </tr>   
                    `                
                });
                $('#products').html(template);
            }
        });
    }

    $(document).on('click', '.product-delete', function() {
        
        if(confirm('Estas seguro de eliminar el producto?')) {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            $.post('backend/product-delete.php', {id}, function (response) {
            console.log(response);
            alert(response);
            fetchProducts();
        });
        }
    });

    $(document).on('click', '.product-item', function() {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        $.post('backend/product-single.php', {id}, function(response) {
            let product = JSON.parse(response);
            $('#name').val(product.nombre);
            $('#description').val(
                    "{ \n" + 
                    '  "precio": ' + product.precio + ",\n" +
                    '  "unidades": ' + product.unidades + "," +
                    '\n  "modelo": ' + ' "' + product.modelo + '",' + 
                    '\n  "marca": ' + ' "' + product.marca + '",'+ 
                    '\n  "detalles": ' + ' "' + product.detalles +  '",' + 
                    '\n  "imagen": ' + ' "' + product.imagen + '"' +  
                    "\n}"
                )
            $('#productId').val(product.id);
            edit = true;
        });
    
    });
});




